﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SpotiApp.API.Migrations
{
    public partial class updatedSongModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Artists");

            migrationBuilder.AddColumn<string>(
                name: "SpotifyID",
                table: "Songs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Uri",
                table: "Songs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SpotifyID",
                table: "Songs");

            migrationBuilder.DropColumn(
                name: "Uri",
                table: "Songs");

            migrationBuilder.CreateTable(
                name: "Artists",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    SpotifyID = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artists", x => x.Id);
                });
        }
    }
}
