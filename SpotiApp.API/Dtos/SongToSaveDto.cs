using System.ComponentModel.DataAnnotations;

namespace SpotiApp.API.Dtos
{
    public class SongToSaveDto
    {

        [Required]
        public string songId { get; set; }

        [Required]
        public string songName { get; set; }

        [Required]
        public string songUri { get; set; }

    }
}