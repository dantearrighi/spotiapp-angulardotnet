namespace SpotiApp.API.Models
{
    public class Song
    {
        public int Id { get; set; }

        public string SpotifyID { get; set; }
        public string Name { get; set; }
        public string Uri { get; set; }

    }
}