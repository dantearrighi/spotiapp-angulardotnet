using System.Threading.Tasks;
using SpotiApp.API.Models;
using System.Collections.Generic;

namespace SpotiApp.API.Data
{
    public class SongsRepository : ISongsRepository
    {
        private readonly DataContext _context;

        public SongsRepository(DataContext context)
        {
            this._context = context;
        }


        public async Task<Song> AddSong(Song song)
        {
            await _context.Songs.AddAsync(song);
            await _context.SaveChangesAsync();
            return song;
        }

        public async Task AddSongs(List<Song> songList)
        {
            await _context.Songs.AddRangeAsync(songList);
            await _context.SaveChangesAsync();

        }

        public Task<Song> GetSong(string id)
        {
            throw new System.NotImplementedException();
        }

        public Task<Song[]> GetSongs(string artistId, string searchTherm)
        {
            throw new System.NotImplementedException();
        }
    }
}