
using SpotiApp.API.Models;
using Microsoft.EntityFrameworkCore;

namespace SpotiApp.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<Song> Songs { get; set; }

        public DbSet<User> Users { get; set; }



    }
}