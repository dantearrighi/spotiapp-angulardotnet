using System.Threading.Tasks;
using SpotiApp.API.Models;
using System.Collections.Generic;

namespace SpotiApp.API.Data
{
    // I'm using Repository Pattern (It's a design pattern)
    public interface ISongsRepository
    {

        Task<Song> GetSong(string id);

        Task<Song[]> GetSongs(string artistId, string searchTherm);

        Task<Song> AddSong(Song song);

        Task AddSongs(List<Song> songList);
    }
}