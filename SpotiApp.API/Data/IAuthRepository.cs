using System.Threading.Tasks;
using SpotiApp.API.Models;

namespace SpotiApp.API.Data
{
    // I'm using Repository Pattern (It's a design pattern)
    public interface IAuthRepository
    {

        Task<User> Register(User user, string password);

        Task<User> Login(string username, string password);

        Task<bool> UserExists(string username);

    }
}