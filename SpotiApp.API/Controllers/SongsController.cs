using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpotiApp.API.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RestSharp;
using SpotiApp.API.Dtos;
using SpotiApp.API.Models;
using SpotiApp.API.Pagination;

namespace SpotiApp.API.Controllers
{

    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SongsController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly ISongsRepository _repo;
        public SongsController(DataContext context, ISongsRepository repo)
        {
            _context = context;
            this._repo = repo;
        }


        #region == SpotiAPP DB ==

        // GET api/songs
        [AllowAnonymous]
        [HttpGet]
        public async Task<PageResult<Song>> GetSongs(int? page, string searchTherm = "")
        {
            var countDetails = await _context.Songs.CountAsync();
            IQueryable<Song> query = _context.Songs;

            if (!String.IsNullOrEmpty(searchTherm) && searchTherm != "undefined")
            {
                query = query.Where(s => s.Name.ToLower().Contains(searchTherm.ToLower()));
            }

            var result = new PageResult<Song>
            {
                Count = countDetails,
                PageIndex = page ?? 1,
                PageSize = 10,
                Items = await query.Skip((page - 1 ?? 0) * 10).Take(10).ToListAsync()
            };
            return result;
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSong(string id)
        {
            //FIXME: Me fui por las ramas, esto tendria que pegarle a spotify y traerme la track... me confundí por no prestarle suficient atencion a el raml y
            // además me entusiasmé con la app y perdí tiempo.
            var song = await _context.Songs.FirstOrDefaultAsync(x => x.SpotifyID == id);
            return Ok(song);
        }

        [HttpPost]
        public async Task<IActionResult> AddSongs([FromBody] List<SongToSaveDto> songsList)
        {
            var newSongs = new List<Song>();
            foreach (SongToSaveDto song in songsList)
            {
                var newSong = new Song
                {
                    SpotifyID = song.songId,
                    Name = song.songName,
                    Uri = song.songUri
                };
                newSongs.Add(newSong);
            }
            await _repo.AddSongs(newSongs);
            return StatusCode(201);
        }

        #endregion

        #region == Spotify web API ==

        // Get token using client id and client secret from Spotify
        [AllowAnonymous]
        [HttpGet("token")]
        public async Task<IActionResult> Token()
        {
            // The access_token is required to get data from Spotify API
            // We need to set 2 params to get the token: client_id & client_secret
            // It should still work even if we change client_id & client_secret values for the ones corresponding to another
            // spotify application (created in developers console)

            // RestSharp NuGet Package is required to excecute the code below:
            var client = new RestSharp.RestClient("https://accounts.spotify.com/api/token");
            client.Timeout = -1;
            var request = new RestSharp.RestRequest(Method.POST);
            // This credentials should be saved on Assets on the server. It's a good practice to have this here.
            // For this test, they're hardcoded.
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "client_credentials");
            request.AddParameter("client_id", "8f8b426ff998464e8ebac31d95cb5f7d");
            request.AddParameter("client_secret", "7dd88311a5264254aa0248580833a17f");
            Task<IRestResponse> t = client.ExecuteAsync(request);
            t.Wait();
            var response = await t;
            return Ok(response.Content);

            // This works if you request to token for the front end.
            // There's no implementation getting and using token inside this API
        }

        private ActionResult GetQuery(string query, string access_token)
        {
            var url = "https://api.spotify.com/v1/";
            Console.WriteLine("url: " + url + query);
            var client = new RestClient(url + query);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + access_token);
            IRestResponse response = client.Execute(request);
            return Ok(response.Content);
        }

        /*
        -- Notes --
        SearchArtist, GetTracks, GetArtist Are not used in this solution.
        */

        // Get all artists by artist name
        [AllowAnonymous]
        [HttpGet("searchartists")]
        public ActionResult SearchArtist(string access_token, string searchTherm)
        {
            return this.GetQuery("search?q=" + searchTherm + "&type=artist", access_token);
        }

        // Get  artist by id
        [AllowAnonymous]
        [HttpGet("getartist")]
        public ActionResult GetArtist(string access_token, string searchTherm)
        {
            return this.GetQuery("artists/" + searchTherm, access_token);
        }

        // Get top tracks by artist id
        [AllowAnonymous]
        [HttpGet("gettracks")]
        public ActionResult GetTracks(string access_token, string searchTherm)
        {
            return this.GetQuery("artists/" + searchTherm + "/toptracks&country=es", access_token);
        }

        // Get song by track id
        [AllowAnonymous]
        [HttpGet("gettracks/{songId}")]
        public ActionResult GetTrack(string access_token, string songId)
        {
            return this.GetQuery("tracks/" + songId, access_token);
            //var client = new RestClient("https://api.spotify.com/v1/tracks/26ZF6lHwjmb9Peq8jdbKUi");
            //client.Timeout = -1;
            // var request = new RestRequest(Method.GET);
            // request.AddHeader("Authorization", "Bearer BQBNpIMKvlYVPo55hwcACJWM7ZqTjWnub1znNzLzlC-hw4ybQRNTL1Cj45t4s1qL1cJy5wV65FIs8zyccSs");
            //IRestResponse response = client.Execute(request);
            //Console.WriteLine(response.Content);
        }


        #endregion



    }
}