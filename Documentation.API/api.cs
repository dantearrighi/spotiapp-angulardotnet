// Template: Client Proxy T4 Template (RAMLClient.t4) version 7.0

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RAML.Api.Core;
using API_Spotify.Api.Models;
using Microsoft.AspNetCore.Mvc.Formatters;
using RAML.Common;

namespace API_Spotify.Api
{
    /// <summary>
    /// Collection of available songs
    /// </summary>
    public partial class Songs
    {
        private readonly ApiClient proxy;

        internal Songs(ApiClient proxy)
        {
            this.proxy = proxy;
        }


        /// <summary>
		/// Collection of available songs. Get a list of songs based on the song title. - /songs
		/// </summary>
		/// <param name="getsongsquery">query properties</param>
        public virtual async Task<ApiResponse> Get(API_Spotify.Api.Models.GetSongsQuery getsongsquery)
        {

            var url = "/songs";
            if(getsongsquery != null)
            {
                url += "?";
                if(getsongsquery.ArtistName != null)
					url += "&artistName=" + Uri.EscapeDataString(getsongsquery.ArtistName);
            }

            url = url.Replace("?&", "?");

            var req = new HttpRequestMessage(HttpMethod.Get, url.StartsWith("/") ? url.Substring(1) : url);
	        var response = await proxy.Client.SendAsync(req);

            return new ApiResponse  
                                            {
                                                RawContent = response.Content,
                                                RawHeaders = response.Headers, 
                                                StatusCode = response.StatusCode,
                                                ReasonPhrase = response.ReasonPhrase,
												SchemaValidation = new Lazy<SchemaValidationResults>(() => new SchemaValidationResults(true), true)
                                            };

        }

        /// <summary>
		/// Collection of available songs. Get a list of songs based on the song title. - /songs
		/// </summary>
		/// <param name="request">API_Spotify.Api.Models.SongsGetRequest</param>
        public virtual async Task<ApiResponse> Get(API_Spotify.Api.Models.SongsGetRequest request)
        {

            var url = "/songs";
            if(request.Query != null)
            {
                url += "?";
                if(request.Query.ArtistName != null)
                    url += "&artistName=" + Uri.EscapeDataString(request.Query.ArtistName);
            }

            url = url.Replace("?&", "?");

            var req = new HttpRequestMessage(HttpMethod.Get, url.StartsWith("/") ? url.Substring(1) : url);

            if(request.RawHeaders != null)
            {
                foreach(var header in request.RawHeaders)
                {
                    req.Headers.TryAddWithoutValidation(header.Key, string.Join(",", header.Value));
                }
            }
	        var response = await proxy.Client.SendAsync(req);
            return new ApiResponse  
                                            {
                                                RawContent = response.Content,
                                                RawHeaders = response.Headers,
                                                StatusCode = response.StatusCode,
                                                ReasonPhrase = response.ReasonPhrase,
												SchemaValidation = new Lazy<SchemaValidationResults>(() => new SchemaValidationResults(true), true)
                                            };
        }

    }

    /// <summary>
    /// Main class for grouping root resources. Nested resources are defined as properties. The constructor can optionally receive an URL and HttpClient instance to override the default ones.
    /// </summary>
    public partial class ApiClient
    {

		public SchemaValidationSettings SchemaValidation { get; private set; } 

        protected readonly HttpClient client;
        public const string BaseUri = "";

        internal HttpClient Client { get { return client; } }




        public ApiClient(string endpointUrl)
        {
            SchemaValidation = new SchemaValidationSettings
			{
				Enabled = true,
				RaiseExceptions = true
			};

			if(string.IsNullOrWhiteSpace(endpointUrl))
                throw new ArgumentException("You must specify the endpoint URL", "endpointUrl");

			if (endpointUrl.Contains("{"))
			{
				var regex = new Regex(@"\{([^\}]+)\}");
				var matches = regex.Matches(endpointUrl);
				var parameters = new List<string>();
				foreach (Match match in matches)
				{
					parameters.Add(match.Groups[1].Value);
				}
				throw new InvalidOperationException("Please replace parameter/s " + string.Join(", ", parameters) + " in the URL before passing it to the constructor ");
			}
            
            if (!endpointUrl.EndsWith("/"))
                endpointUrl += "/";

            client = new HttpClient {BaseAddress = new Uri(endpointUrl)};
        }

        public ApiClient(HttpClient httpClient)
        {
            if(httpClient.BaseAddress == null)
                throw new InvalidOperationException("You must set the BaseAddress property of the HttpClient instance");

            client = httpClient;

			SchemaValidation = new SchemaValidationSettings
			{
				Enabled = true,
				RaiseExceptions = true
			};
        }

        

        public virtual Songs Songs
        {
            get { return new Songs(this); }
        }
                


		public void AddDefaultRequestHeader(string name, string value)
		{
			client.DefaultRequestHeaders.Add(name, value);
		}

		public void AddDefaultRequestHeader(string name, IEnumerable<string> values)
		{
			client.DefaultRequestHeaders.Add(name, values);
		}

        // Root methods






    }

} // end namespace









namespace API_Spotify.Api.Models
{
    public partial class  SongListItem 
    {

		[JsonProperty("string,")]
        public object SongId { get; set; }


		[JsonProperty("songTitle")]
        public string SongTitle { get; set; }


    } // end class

    public partial class  ExternalUrls 
    {

		[JsonProperty("spotify")]
        public string Spotify { get; set; }


    } // end class

    public partial class  Song 
    {

		[JsonProperty("href")]
        public string Href { get; set; }


		[JsonProperty("id")]
        public string Id { get; set; }


		[JsonProperty("is_local")]
        public bool Is_local { get; set; }


		[JsonProperty("is_playable")]
        public bool Is_playable { get; set; }


		[JsonProperty("name")]
        public string Name { get; set; }


		[JsonProperty("preview_url")]
        public string Preview_url { get; set; }


		[JsonProperty("track_number")]
        public int Track_number { get; set; }


		[JsonProperty("type")]
        public string Type { get; set; }


		[JsonProperty("uri")]
        public string Uri { get; set; }


		[JsonProperty("disc_number")]
        public int Disc_number { get; set; }


		[JsonProperty("duration_ms")]
        public int Duration_ms { get; set; }


		[JsonProperty("explicit")]
        public bool Explicit { get; set; }


		[JsonProperty("external_urls")]
        public ExternalUrls External_urls { get; set; }


        public IList<object> Artists { get; set; }


    } // end class

    public partial class  GetSongsQuery 
    {

        /// <summary>
        /// The name of the artist (case insensitive and doesn&apos;t need to match the whole name of the artist)
        /// </summary>
        [Required]
		[JsonProperty("artistName")]
        public string ArtistName { get; set; }


    } // end class

    /// <summary>
    /// Request object for method Get of class Songs
    /// </summary>
    public partial class SongsGetRequest : ApiRequest
    {
        public SongsGetRequest(GetSongsQuery Query = null)
        {
            this.Query = Query;
        }


        /// <summary>
        /// Request query string properties
        /// </summary>
        public GetSongsQuery Query { get; set; }

    } // end class


} // end Models namespace
