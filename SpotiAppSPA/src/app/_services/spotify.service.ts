import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  sptoken: string;

  constructor(private http: HttpClient) {
  }

  /* 
   FIXME -- Notes --
   Would be better if we have two services, one for SpotiApp API Functions
   and the other for Spotify WEB API Functions
  */

  // #region Token Functions

  getSPWebAPIQuery(query: string) {

    const url = `https://api.spotify.com/v1/${query} `;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.getToken()
    });

    return this.http.get(url, { headers });
  }

  getToken() {
    // If a valid token doesn't exist, then request it.
    if (!this.sptoken) {
      return this.http.get('http://localhost:5000/api/songs/token').subscribe(data => {
        // Get the access_token value from the data response
        this.sptoken = data['access_token'];
        // Save it in local storage
        localStorage.setItem('sptoken', this.sptoken);
      });
    } else {
      // If already have a valid token, then return it.
      return this.sptoken;
    }
  }

  refreshSpotifyToken(): any {
    localStorage.removeItem('sptoken');
    this.getToken();
  }

  getTokenDontUseThis(): any {

    // IMPORTANT:
    // For test purpose, this was hardcoded in the front end. This shouldn't be here for security reasons.

    // Set the params required to get token via Spotify WEB API
    const params = new HttpParams()
      .set('grant_type', "client_credentials")
      .set('client_id', "8f8b426ff998464e8ebac31d95cb5f7d")
      .set('client_secret', "7dd88311a5264254aa0248580833a17f");

    // If a valid token doesn't exist, then request it.
    if (!this.sptoken) {
      return this.http.post('https://accounts.spotify.com/api/token', params).subscribe(data => {
        // Get the access_token value from the data response
        this.sptoken = data['access_token'];
        // Save it in local storage
        localStorage.setItem('sptoken', this.sptoken);
      });
    } else {
      // If already have a valid token, then return it.
      return this.sptoken;
    }
  }

  //#endregion

  // #region SpotiApp API Functions

  // Get Songs from spotiapp DataBase
  getSongs(pageNumber?: number, artistName?: string): any {
    console.log(artistName);
    if (Number.isInteger(pageNumber)) {
      return this.http.get(`http://localhost:5000/api/songs?page=${pageNumber}&searchTherm=${artistName}`).pipe(map((data: any) => data));
    }
    return this.http.get(`http://localhost:5000/api/songs?searchTherm=${artistName}`).pipe(map((data: any) => data));
  }

  // Get a single Song by id from spotiapp DataBase
  getSong(songId: string): any {
    return this.http.get(`http://localhost:5000/api/songs/${songId}`).pipe(map((data: any) => data));
  }

  // Save songs into spotiapp Database
  addSongs(songs: any) {

    const token = localStorage.getItem('token');
    const headers = new HttpHeaders().set("Authorization", "Bearer " + token);
    return this.http.post('http://localhost:5000/api/songs', songs, { headers });

  }

  //#endregion

  // #region Spotify WEB API Functions

  getArtists(searchTherm: string) {
    return this.getSPWebAPIQuery(`search?q=${searchTherm}&type=artist`)
      .pipe(map((data: any) => data['artists'].items))
  }

  getArtist(idArtist: string) {
    return this.getSPWebAPIQuery(`artists/${idArtist}`);
  }

  getAlbums(idArtist: string) {
    return this.getSPWebAPIQuery(`artists/${idArtist}/albums`)
      .pipe(map((data: any) => data['items']));
  }

  getAlbumTracks(idAlbum: string) {
    return this.getSPWebAPIQuery(`albums/${idAlbum}/tracks`)
      .pipe(map((data: any) => data['items']));
  }

  getTopTracks(id: string) {
    return this.getSPWebAPIQuery(`artists/${id}/toptracks?country=es`)
      .pipe(map((data: any) => data['tracks']));
  }

  //#endregion


}