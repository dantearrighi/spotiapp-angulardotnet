import { Component, OnInit } from '@angular/core';

import { SpotifyService } from '../_services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: []
})
export class SearchComponent implements OnInit {

  tracks: any[] = [];
  artists: any[] = [];
  loading: boolean;
  error: boolean;
  errorMessage: string;

  constructor(private spotifyService: SpotifyService) { }

  ngOnInit() {
    this.spotifyService.getToken();
  }

  searchArtist(searchTherm: string) {
    if (searchTherm.length >= 2) {
      this.loading = true;
      this.artists = [];

      // Use the spotifyService injected in songs component to get artists
      this.spotifyService.getArtists(searchTherm)
        .subscribe((data: any) => {
          if (data) {
            this.artists = data;
          }
          this.loading = false;
        }, (errorMessage) => {
          this.error = true;
          this.errorMessage = errorMessage.error.error.message;
          this.spotifyService.refreshSpotifyToken();
        });
    } else {
      this.artists = [];
    }

  }




}
