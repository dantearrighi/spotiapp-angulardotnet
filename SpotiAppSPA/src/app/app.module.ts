import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';

import { NavComponent } from '../app/shared/nav/nav.component';
import { AuthService } from './_services/auth.service';
import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { ArtistsComponent } from './artists/artists.component';
import { HomeComponent } from './home/home.component';
import { MysongsComponent } from './mysongs/mysongs.component';
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { NoimagePipe } from './pipes/noimage.pipe';
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { CardsComponent } from './shared/cards/cards.component';
import { LoadingComponent } from './shared/loading/loading.component';

// Pipes
@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      HomeComponent,
      CardsComponent,
      RegisterComponent,
      SearchComponent,
      ArtistsComponent,
      LoadingComponent,
      NoimagePipe,
      DomseguroPipe,
      MysongsComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      RouterModule.forRoot(ROUTES),
      NgxPaginationModule
   ],
   providers: [
      AuthService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
