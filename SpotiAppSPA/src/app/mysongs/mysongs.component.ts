import { Component, OnInit } from '@angular/core';

import { SpotifyService } from '../_services/spotify.service';
import { SongModel } from '../models/song.model';

@Component({
  selector: 'app-mysongs',
  templateUrl: './mysongs.component.html',
  styleUrls: []
})
export class MysongsComponent implements OnInit {

  searchTherm = '';
  loading: boolean;

  mysongs: any[] = [];
  error: boolean;
  errorMessage: string;
  noSongsMessage = 'No songs found in the DB. Go to Home, search for your favourite artist and add songs!';

  songSelected: any = {};

  public songs: SongModel[];
  public pageNumber = 1;
  public Count: number;

  constructor(private spotifyService: SpotifyService) {
    this.getMySongs();
  }

  ngOnInit() {
  }

  getMySongs() {
    // Use the spotifyService injected in mysongs component to get songs
    this.spotifyService.getSongs()
      .subscribe((data: any) => {
        if (data) {
          this.mysongs = data.items;
          this.pageNumber = data.pageIndex;
          this.Count = data.count;
          if (this.Count === 0) {
            this.error = true;
          }
        }
      }, (errorMessage) => {

        this.error = true;
        this.errorMessage = errorMessage.error.error.message;
        this.spotifyService.refreshSpotifyToken();
      });
  }

  getMoreSongs(pageNumber) {
    this.spotifyService.getSongs(pageNumber)
      .subscribe((data: any) => {
        if (data) {
          this.mysongs = data.items;
          this.pageNumber = data.pageIndex;
          this.Count = data.count;
        }
      }, (errorMessage) => {
        this.error = true;
        this.errorMessage = errorMessage.error.error.message;
        this.spotifyService.refreshSpotifyToken();
      });
  }

  searchSongs(searchTherm, pageNumber?) {
    if (searchTherm.length >= 2) {
      this.loading = true;
      this.mysongs = [];
      this.spotifyService.getSongs(pageNumber, searchTherm)
        .subscribe((data: any) => {
          if (data) {
            this.mysongs = data.items;
            this.pageNumber = data.pageIndex;
            this.Count = data.count;
          }
          this.loading = false;
        }, (errorMessage) => {
          this.error = true;
          this.errorMessage = errorMessage.error.error.message;
          this.spotifyService.refreshSpotifyToken();
        });
    } else {
      this.getMySongs();
    }

  }

  getSong(songId: string) {
    this.spotifyService.getSong(songId).subscribe(data => {
      this.songSelected = data;
      console.log(this.songSelected);
    })
  }
}



