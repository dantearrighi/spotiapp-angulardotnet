import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../_services/auth.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  // I prefer to have a specific model for the user. Considering this is for the purpose of the challenge,
  // many models are not typed (although I would prefer that they were).
  model: any = {};

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.model).subscribe(next => {
      console.log('Logged in successfully');
    }, error => {
      console.log('Failed to login');
    });
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !!token; // If there's something in this token, return true.
  }

  logout() {
    // Remove tokens from local storage
    localStorage.removeItem('token');
    localStorage.removeItem('sptoken');
    console.log('Logged out! Hope to see you soon');
  }
}
