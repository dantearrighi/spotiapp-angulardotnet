import { Routes } from '@angular/router';

import { ArtistsComponent } from './artists/artists.component';
import { HomeComponent } from './home/home.component';
import { MysongsComponent } from './mysongs/mysongs.component';

export const ROUTES: Routes = [

    { path: 'home', component: HomeComponent },
    { path: 'mysongs', component: MysongsComponent },
    { path: 'artists', component: ArtistsComponent },
    { path: 'artists/:id', component: ArtistsComponent },
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: '**', pathMatch: 'full', redirectTo: 'home' }
]