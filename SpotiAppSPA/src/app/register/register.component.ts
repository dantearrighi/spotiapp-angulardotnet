import { Component, EventEmitter, Output } from '@angular/core';

import { AuthService } from '../_services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: []
})
export class RegisterComponent {

  @Output() cancelRegister = new EventEmitter();

  model: any = {};


  constructor(private authService: AuthService) { }


  register() {
    this.authService.register(this.model).subscribe(() => {
      console.log('Registration successful');
      this.cancelRegister.emit(false);
    }, error => {
      console.log(error);
    });
  }

  cancel() {
    this.cancelRegister.emit(false);
    console.log('Registration cancelled');
  }
}
