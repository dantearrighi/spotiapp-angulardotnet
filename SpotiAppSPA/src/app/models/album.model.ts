export class AlbumModel {

    'albumId': string;
    'albumName': string;
    'albumUri': string;
    'albumImages': string;

}