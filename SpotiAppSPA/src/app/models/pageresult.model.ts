/*
    This model is useful to handle the pagination component handled by ngx-pagination
*/
class PageResult<T>
{
    count: number;
    pageIndex: number;
    pageSize: number;
    items: T[];
}