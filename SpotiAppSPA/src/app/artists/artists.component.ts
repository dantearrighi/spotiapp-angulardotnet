import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SpotifyService } from '../_services/spotify.service';
import { SongModel } from '../models/song.model';

@Component({
  selector: 'app-artists',
  templateUrl: './artists.component.html',
  styleUrls: []
})
export class ArtistsComponent {

  loadingArtist: boolean;
  artist: any = {};

  albums: any[] = [];
  topTracks: any[] = [];
  artistTracks: any[] = [];

  songsToSave: SongModel[] = [];

  /*
  This is just a flag to disable the "Save Songs" button.
  It's ok for this challenge app, but I understand it doesn't have much sense to do things this way
  in a real app. A better approach would be to validate against DB if this songs were already saved
  in order to not to persist duplicated songs.
  */
  songsSaved = false;

  constructor(
    private router: ActivatedRoute,
    private spotifyService: SpotifyService) {

    this.router.params.subscribe(params => {
      this.getArtistData(params['id']);
      this.getArtist(params['id']);
    });
  }

  getArtistData(idArtist: string) {
    this.getAlbums(idArtist);
    this.getTopTracks(idArtist);
  }

  getAlbums(idArtist: string) {
    this.spotifyService.getAlbums(idArtist)
      .subscribe(albums => {
        this.albums = albums;
      })
  }

  getTopTracks(id: string) {
    this.spotifyService.getTopTracks(id)
      .subscribe(tracks => {
        this.topTracks = tracks;
      });
  }

  getArtist(idArtist: string) {
    this.loadingArtist = true;
    this.spotifyService.getArtist(idArtist)
      .subscribe(artist => {
        this.artist = artist;
        this.loadingArtist = false;
        this.albums.forEach(album => {
          this.getAlbumTracks(album.id);
        });
      });
  }

  getAlbumTracks(idAlbum: string) {
    let tempSongs: SongModel[] = [];
    return this.spotifyService.getAlbumTracks(idAlbum)
      .subscribe(tracksData => {
        tempSongs = tracksData;
        this.artistTracks = this.artistTracks.concat(tempSongs);
        this.createSongsToSave();
      })
  }

  createSongsToSave() {
    this.songsToSave = this.artistTracks.map((track) => {
      // Map tracks from spotify into SpotiApp Song Model
      let songItem: SongModel = {
        'songId': track.id,
        'songName': track.name,
        'songUri': track.uri
      };
      return songItem;
    });

  }

  saveSongs() {
    this.spotifyService.addSongs(this.songsToSave).subscribe(() => {
      this.songsSaved = true;
    }, error => {
      console.log(error);
    })
  }




}


