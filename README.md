<h2>Frontend: Angular || Backend: Dotnet Core</h2>

<h3>Spotify Web API</h3>
SpotiApp gets data from Spotify Web API

<h3>SpotiApp Backend API</h3>

Developed with Microsoft Dotnet Core 3.0.0

<h3>SpotiApp Frontend</h3>

Developed with Angular Framework - CLI 10.0.0